import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { GoogleSigninService } from '../googel-signin.service';

@Component({
  selector: 'app-anmelden',
  templateUrl: './anmelden.component.html',
  styleUrls: ['./anmelden.component.css']
})
export class AnmeldenComponent implements OnInit {
  user: gapi.auth2.GoogleUser | undefined
  constructor(private signInService : GoogleSigninService, private ref : ChangeDetectorRef ) {


  }

  ngOnInit(): void {
      this.signInService.observable().subscribe(user => {
        this.user =user
        this.ref.detectChanges()
      })


  }

  signIn() {
    this.signInService.signin()
  }

  signOut () {
    this.signInService.signOut()
  }
}
  

  
  


