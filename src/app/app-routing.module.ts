import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchriftComponent } from './schrift/schrift.component';
import { ZieleComponent } from './ziele/ziele.component';
import { TopComponent } from './top/top.component';
import { ServicComponent } from './servic/servic.component';
import { NewsComponent } from './news/news.component';
import { MeinungComponent } from './meinung/meinung.component';
import { KundenComponent } from './kunden/kunden.component';
import { BuchungComponent } from './buchung/buchung.component';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { ImpresumComponent } from './impresum/impresum.component';
import { KontaktComponent } from './kontakt/kontakt.component';
import { AnmeldenComponent } from './anmelden/anmelden.component';
import { KarteComponent } from './karte/karte.component';
const routes: Routes = [{ path: 'schrift', component: SchriftComponent},{ path: 'ziele', component: ZieleComponent},{ path: 'top', component: TopComponent},{ path: 'servic', component: ServicComponent}, {path: 'news', component: NewsComponent}, {path: 'meinung', component: MeinungComponent} ,{path: 'kunden', component: KundenComponent}, {path: 'buchung', component: BuchungComponent},{path: 'datenschutz', component: DatenschutzComponent},{path: 'impresum', component: ImpresumComponent},{path: 'kontakt', component: KontaktComponent},{ path: 'anmelden', component: AnmeldenComponent},{path: 'karte', component: KarteComponent}]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [SchriftComponent,ZieleComponent,TopComponent,ServicComponent,NewsComponent,MeinungComponent,KundenComponent,BuchungComponent,DatenschutzComponent,ImpresumComponent,KontaktComponent,AnmeldenComponent,KarteComponent]