import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NaviComponent } from './navi/navi.component';
import { SchriftComponent } from './schrift/schrift.component';
import { ServicComponent } from './servic/servic.component';
import { ZieleComponent } from './ziele/ziele.component';
import { BuchungComponent } from './buchung/buchung.component';
import { MeinungComponent } from './meinung/meinung.component';
import { NewsComponent } from './news/news.component';
import { KundenComponent } from './kunden/kunden.component';
import { TopComponent } from './top/top.component';
import { KontaktComponent } from './kontakt/kontakt.component';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { ImpresumComponent } from './impresum/impresum.component';
import { AnmeldenComponent } from './anmelden/anmelden.component';
import { KarteComponent } from './karte/karte.component';

@NgModule({
  declarations: [
    AppComponent,
    NaviComponent,
    SchriftComponent,
    ServicComponent,
    ZieleComponent,
    BuchungComponent,
    MeinungComponent,
    NewsComponent,
    KundenComponent,
    TopComponent,
    KontaktComponent,
    DatenschutzComponent,
    ImpresumComponent,
    AnmeldenComponent,
    KarteComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
