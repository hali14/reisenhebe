import { TestBed } from '@angular/core/testing';

import { GoogleSigninService } from './googel-signin.service';

describe('GoogelSigninService', () => {
  let service: GoogleSigninService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoogleSigninService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
