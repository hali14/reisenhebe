import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeinungComponent } from './meinung.component';

describe('MeinungComponent', () => {
  let component: MeinungComponent;
  let fixture: ComponentFixture<MeinungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeinungComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MeinungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
